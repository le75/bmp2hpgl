import argparse
from skimage.io import imread


def main(input_file, x_spacing=25, y_spacing=25, force=2, pen=1, velocity=80):
    im = imread(input_file, as_gray=True)
    cmd = ["IN", f"VS{velocity}", f"SP{pen}", f"FS{force}"]

    for (x, row) in enumerate(im):
        px = x * x_spacing

        is_pen_down = False

        # moving the pen up and to the beggining of the row
        cmd.append(f"PU")
        cmd.append(f"PA{px},0")

        # looping through each pixel of the row
        for (y, val) in enumerate(row, start=0):
            py = y * y_spacing

            # if a black pixel, and previous pixel wasn't black:
            # move pen and do a Pen Down
            if val == 0 and is_pen_down == False:
                is_pen_down = True
                cmd.append(f"PA{px},{py}")
                cmd.append(f"PD")

            # if a white pixel, and previous pixel was black:
            # move pen and do a Pen Up
            elif val != 0 and is_pen_down == True:
                is_pen_down = False
                cmd.append(f"PA{px},{py}")
                cmd.append(f"PU")

        # moving the pen to finish the row
        cmd.append(f"PA{px},{py + y_spacing}")

    # Reinitialing the plotter
    cmd.append("PU0,0")
    cmd.append("IN;")

    print(";\n".join(cmd))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Converts a bitmap image to horizontal hatches in hpgl/plt format."
    )
    parser.add_argument("input_file", help="The path of the input bitmap")
    parser.add_argument(
        "-x",
        "--x-spacing",
        default="25",
        type=int,
        help="Hatch X spacing (default: 25)",
    )
    parser.add_argument(
        "-y",
        "--y-spacing",
        default="25",
        type=int,
        help="Hatch Y spacing (default: 25)",
    )
    parser.add_argument(
        "-f",
        "--force",
        default="2",
        type=int,
        help="Force (default: 2)",
    )
    parser.add_argument(
        "-p",
        "--pen",
        default="1",
        type=int,
        help="Pen (default: 1)",
    )
    parser.add_argument(
        "-v",
        "--velocity",
        default="80",
        type=int,
        help="Velocity (default: 80)",
    )
    args = parser.parse_args()

    main(args.input_file, x_spacing=args.x_spacing, y_spacing=args.y_spacing)
