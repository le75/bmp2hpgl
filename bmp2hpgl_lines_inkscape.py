from skimage.io import imread
im = imread("img/earth_from_moon.png")

cmd = ["IN", "VS80", "SP1"]

for (x, row) in enumerate(im):
    px = x * 25
    cmd.append(f"PU{px},0")
    for (y, val) in enumerate(row):
        py = y * 25

        if val == 0:
            cmd.append(f"PD{px},{py}")
        else:
            cmd.append(f"PU{px},{py}")

cmd.append("PU0,0")
cmd.append("IN;")

print(";\n".join(cmd))
