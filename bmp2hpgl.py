import argparse
from skimage.io import imread


def main(input_file, x_spacing=25, y_spacing=25, force=2, pen=1, velocity=80):
    im = imread(input_file, as_gray=True)
    cmd = ["IN", f"VS{velocity}", f"SP{pen}", f"FS{force}"]

    for (x, row) in enumerate(im):
        for (y, val) in enumerate(row):
            if val == 0:
                cmd.append(f"PU{x*x_spacing},{y*y_spacing}")
                cmd.append("PD")

    cmd.append("PU0,0")
    cmd.append("IN;")

    print(";\n".join(cmd))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Converts a bitmap image to dotted matrix in hpgl/plt format."
    )
    parser.add_argument("input_file", help="The path of the input bitmap")
    parser.add_argument(
        "-x",
        "--x-spacing",
        default="25",
        type=int,
        help="Hatch X spacing (default: 25)",
    )
    parser.add_argument(
        "-y",
        "--y-spacing",
        default="25",
        type=int,
        help="Hatch Y spacing (default: 25)",
    )
    parser.add_argument(
        "-f",
        "--force",
        default="2",
        type=int,
        help="Force (default: 2)",
    )
    parser.add_argument(
        "-p",
        "--pen",
        default="1",
        type=int,
        help="Pen (default: 1)",
    )
    parser.add_argument(
        "-v",
        "--velocity",
        default="80",
        type=int,
        help="Velocity (default: 80)",
    )
    args = parser.parse_args()

    main(args.input_file, x_spacing=args.x_spacing, y_spacing=args.y_spacing, force=args.force, pen=args.pen, velocity=args.velocity)
